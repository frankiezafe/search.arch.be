import requests
import time
import shutil
import os

# CONFIGURATION

TARGET = 'cache'

BASE_URL = 'https://search.arch.be/imageserver/getpic.php?%SID%/%LID%/%LID%_%BID%_000/%LID%_%BID%_000_0_0001.jp2&'

SHORT_START_AT = 1
SHORT_END_AT = 999

LONG_START_AT = 1
LONG_END_AT = 5000

SUB_START_AT = 1
SUB_END_AT = 2000

# PROCESS

def do_request( url ):
	retry = 0
	s=time.time()
	r = None
	while retry < 5:
		try: 
			r = requests.get(url,stream=True,timeout=5)
			break
		except: 
			print( 'error/time out:', url, 'try:', retry )
			r = None
			retry += 1
			time.sleep(2)
	return r

for s in range( SHORT_START_AT, SHORT_END_AT+1 ):
	
	shortid = str(s)
	while len(shortid) < 3:
		shortid = '0'+shortid
	
	if os.path.exists(os.path.join(TARGET,'.'+shortid)):
		continue
	
	sid_exists = False
	
	for l in range( LONG_START_AT, LONG_END_AT+1 ):
		
		longid = str(l)
		while len(longid) < 4:
			longid = '0'+longid
		longid = shortid+'_'+longid+'_000'
		
		if os.path.exists(os.path.join(TARGET,'.'+longid)):
			continue
		
		print( 'scanning:', longid )
		lid_exists = False
		
		for i in range( SUB_START_AT, SUB_END_AT+1 ):
			
			bid = str(i)
			while len(bid) < 5:
				bid = '0'+bid
			
			current_id = longid+'_'+bid
			if os.path.exists(os.path.join(TARGET,current_id)):
				lid_exists = True
				sid_exists = True
				break
			
			url = BASE_URL.replace('%SID%',shortid).replace('%LID%',longid).replace('%BID%',bid) + '1'
			r = do_request(url)
			
			if r == None or r.status_code != 200:
				print( 'failed:', longid, bid )
				break
			
			lid_exists = True
			sid_exists = True
			
			folder = os.path.join(TARGET,current_id)
			if not os.path.exists(folder):
				os.makedirs(folder)
			r.raw.decode_content = True
			with open(os.path.join(folder,'00001.jpg'),'wb') as f:
				shutil.copyfileobj(r.raw, f)
			print( '\t', current_id )
		
		if sid_exists and not lid_exists:
			ep = os.path.join(TARGET,'.'+longid)
			e = open( ep, 'w' )
			e.close()
		
		if not sid_exists:
			break
	
	if not sid_exists:
		ep = os.path.join(TARGET,'.'+shortid)
		e = open( ep, 'w' )
		e.close()

'''
exit()

for i in range( SUB_START_AT, SUB_END_AT+1 ):
	n = str(i)
	while len(n) < 5:
		n = '0'+n
	current_id = LONG_ID + '_' + n
	# try to get the thumbnail for this id
	url = BASE_URL.replace('%SID%',SHORT_ID).replace('%LID%',LONG_ID).replace('%BID%',n) + '1'
	print( "request to", url )
	
	retry = 0
	s=time.time()
	r = None
	while retry < 5:
		try: 
			r = requests.get(url, stream = True, timeout=5)
			break
		except: 
			print( 'error/time out:', url, 'try:', retry )
			r = None
			retry += 1
			time.sleep(2)
	
	# yes!
	ipath = os.path.join(current_id,'001.jpg')
	if r != None and r.status_code == 200:
		folder = os.path.join(TARGET,current_id)
		if not os.path.exists(folder):
			os.makedirs(folder)
		r.raw.decode_content = True
		with open(os.path.join(folder,'00001.jpg'),'wb') as f:
			shutil.copyfileobj(r.raw, f)
		print('success:',ipath, ':', (i-START_AT), '/', (END_AT-START_AT))
	else:
		print('failed:',ipath, ':', (i-START_AT), '/', (END_AT-START_AT))
'''

# arche.be blueprint retriever

a set of python scripts to reconstruct HD version of the blueprint 
available on [search.arch.be](https://search.arch.be/)

# installation

requires python3

```
pip3 install Pillow
```

# process

## scanner.py

this script tries to extract the thumbnail of a serie of blueprint
related to a database entry

it will create a folder for each blueprint in **cache** folder and 
save the thumbnail in it

```
$ python3 scanner.py
```

### configuration

connect to [search.arch.be](https://search.arch.be/), locate the 
blueprint you want to extract and launch the viewer

once launched, press `F12` (in firefox) to open the code inspector

go to **network** tab, uncheck everything except **Images** and sort the results by **File**

reload the page with `CTRL+F5`

back in the inspector, search for this pattern

```
https://search.arch.be/imageserver/getpic.php?[some ID].jp&[xxx]
```

in **scanner.py**, adapt:

- **SHORT_START_AT** and **SHORT_END_AT**
- **LONG_START_AT** and **LONG_END_AT**
- **SUB_START_AT** and **SUB_END_AT**

the script will generates all the possible combinaisons of path within
these bounds and try to get the first image of the folder

if it fails, it will generates a error file for each groups

## collector.py

this script download all availble tiles for blueprint available in
**cache** folder

it will generate valid urls based on the folder name and store tiles
by number

failed attempts will be stored as `.[xxxx].jpg`

after 10 failed attempt, script will create a **done** file in folder 
and jump to next folder

it is limited to 5000 tiles, if you need higher, adapt the **END_AT** 
variable in the script

to reprocess a folder, **remove the done file**: the reference will be
reprocessed

```
$ python3 collector.py
```

### configuration

if you want to restrict the script to a specific range:

- **FILTER**: first characters of the cache folder

script verifies tiles already downloaded

you can follow the process in terminal

## janitor.py

`collector.py` is something messing while writing the image on the drive

the job of this script is to: 

- anaylse every images in cache folder;
- detect corrupted files;
- and delete them as well as the hd folder containing the reconstructions

run it after each massive retrieval to fix a majority of bugs while 
reconstruction

```
$ python3 janitor.py
```

## reconstrut.py

this script rebuild images from tiles available in **cache**

it collects all the tiles form **cache** folders containing a 
**done** file

reconstruction will appears in folder **hd**

it tries to guess the correct size of the grid, by using the ratio
of the thumbnail and after that the last reconstruction

errors might occurs in case of a missing tile for instance, so check 
`hd/report` once the script stops to fix them

after reconstruction of blueprint, the script generates a 
**catalog.html** in root folder; open it to browse the hd files


```
$ python3 reconstrut.py
```

### configuration

if you want to restrict the script to a specific range:

- **FILTER**: first characters of the cache folder

# notes

- it seems that `510_2313_000` is an identifier for the building
- the blueprint number is stored in the 4th block: `510_2313_000_[blueprint id]_000`; the block have 5 digits
- it is public urls


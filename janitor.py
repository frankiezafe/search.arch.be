import os
import shutil
from PIL import Image

# CONFIGURATION

SOURCE = 'cache'
TARGET = 'hd'
FORCE = False

# FUNCTIONS

class cleaning_info:
	def __init__(self,path,dir):
		self.path = path
		self.dir = dir
		self.file_count = 0
		self.dirty_files = []
		self.rescan = False
	def dump(self):
		print( 'cleaning_info' )
		print( 'valid:', self.valid )
		print( 'path:', self.path )
		print( 'dir:', self.dir )
		print( 'dirty_files:', self.dirty_files )
		print( 'rescan:', self.rescan )

def analyse_folder( path, dir ):
	
	print( 'analysing:' + path )
	
	cf = cleaning_info(path,dir)
	
	if not FORCE and os.path.isfile(os.path.join(path,'finished')):
		return cf
	if not os.path.isfile(os.path.join(path,'done')):
		return cf
	
	files = os.listdir(path)
	cf.file_count = len(files)
	
	for f in files:
		fp = os.path.join(path,f)
		if os.path.isfile(fp):
			prefix = f[0]
			ext = f[-4:]
			if prefix != '.' and ext == '.jpg':
				try:
					im = Image.open(fp)
					if im.size[0] == 0 or im.size[1] == 0:
						cf.dirty_files.append( fp )
						cf.rescan = True
				except:
					cf.dirty_files.append( fp )
					cf.rescan = True
				err_fp = os.path.join(path,'.'+f)
				if os.path.isfile(err_fp):
					if not err_fp in cf.dirty_files:
						cf.dirty_files.append( err_fp )
			elif prefix == '.':
				if not fp in cf.dirty_files:
					cf.dirty_files.append(fp)
			elif f == 'done':
				cf.dirty_files.append(fp)
	cf.dirty_files.sort()
	return cf

def clean_folder( cf ):
	
	dparts = cf.dir.split('_')
	tp = os.path.join(TARGET, dparts[0]+'_'+dparts[1]+'_'+dparts[2], dparts[3])
	if os.path.isdir(tp):
		print( 'cleaning target:', tp )
		shutil.rmtree(tp)
	
	for f in cf.dirty_files:
		print( 'error:', f )
		os.remove(f)

# PROCESS

if not os.path.exists(SOURCE):
	exit()

dir_list = os.listdir(SOURCE)
dir_list.sort()

for d in dir_list:
	dp = os.path.join(SOURCE, d)
	if os.path.isdir(dp):
		cf = analyse_folder( dp, d )
		if cf.rescan == True:
			clean_folder( cf )
		if cf.file_count > 1:
			f = open( os.path.join(dp,'finished'), 'w' )
			f.close()

import requests
import time
import shutil
import sys
import os

# CONFIGURATION

SOURCE = 'cache'
BASE_URL = 'https://search.arch.be/imageserver/getpic.php?%SID%/%LID%/%LID%_%BID%_000/%LID%_%BID%_000_0_0001.jp2&'
FILTER = '510_2313'

# PROCESS

dir_list = os.listdir(SOURCE)
dir_list.sort()

START_AT = 1
END_AT = 10000
MAX_FAILED = 10

for d in dir_list:
	
	if len(FILTER) > 0 and d.find(FILTER) == -1:
		continue
	
	components = d.split('_')
	if len(components) != 4:
		continue
	short_id = components[0]
	long_id = components[0]+'_'+components[1]+'_'+components[2]
	blueprint_id = components[3]
	
	jpegs = []
	f_path = os.path.join(SOURCE,d)
	f_content = os.listdir(f_path)
	f_content.sort()
	do_scan = True
	
	for f in f_content:
		if f[-4:] == '.jpg':
			jpegs.append( f )
		if f == 'done':
			print( 'folder '+f_path+' is done' )
			do_scan = False
			break
	
	if do_scan == False:
		continue
	
	# start scan
	failed = 0
	for i in range( START_AT, END_AT+1 ):
		
		attempt_retrieve = True
		
		jpg_path = str(i)
		while len(jpg_path) < 5:
			jpg_path = '0'+jpg_path
		jpg_path += '.jpg'
		if jpg_path in jpegs:
			#print( jpg_path+' already downloaded' )
			attempt_retrieve = False
			continue
		if '.'+jpg_path in jpegs:
			#print( jpg_path+' already tested' )
			failed += 1
			attempt_retrieve = False
			continue
		
		if attempt_retrieve == True:
		
			jpg_full_path = os.path.join(f_path,jpg_path)
			
			url = BASE_URL.replace('%SID%',short_id).replace('%LID%',long_id).replace('%BID%',blueprint_id) + str(i)
			
			retry = 0
			s = time.time()
			r = None
			while retry < 5:
				try: 
					r = requests.get(url, stream = True, timeout=5)
					break
				except: 
					print( 'error/time out:', url, 'try:', retry )
					r = None
					retry += 1
					time.sleep(2)
			
			success = False
			
			if r != None and r.status_code == 200:
				r.raw.decode_content = True
				with open(jpg_full_path,'wb') as f:
					try:
						shutil.copyfileobj(r.raw, f)
						success = True
					except:
						failed += 1
						pass
				if success == True:
					fs = os.path.getsize(jpg_full_path)
					if fs < 2:
						os.remove( jpg_full_path )
						success = False
					else:
						print('success:',jpg_full_path+':', str(i+1-START_AT)+'/'+str(END_AT-START_AT))
						failed = 0
						success = True
			
			if success == False:
				done_path = os.path.join(f_path,'.'+jpg_path)
				f = open(done_path,"w")
				f.close()
				print('failed:',done_path, ':', (i+1-START_AT), '/', (END_AT-START_AT))
				failed += 1
		
		if failed >= MAX_FAILED:
			print( "max fails reached, folder "+f_path+" done" )
			break
	
	# flagging folder as done
	done_path = os.path.join(f_path,'done')
	f = open(done_path,"w")
	f.close()

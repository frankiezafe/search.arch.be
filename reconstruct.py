import os
import math
from PIL import Image

# CONFIGURATION

SOURCE = 'cache'
TARGET = 'hd'
TARGET_EXT = '.jpg'
TILE = 256
FILTER = '510_2313'

# PROCESS

class reconstruct_info:
	
	def __init__(self,unique):
		self.unique = unique
		self.cache = os.path.join(SOURCE,unique)
		ps = unique.split('_')
		self.hd = os.path.join(TARGET,ps[0]+'_'+ps[1]+'_'+ps[2], ps[3])
		self.size = [0,0]
		self.grid = [1,1]
		self.tiles = []
		self.resolutions = []
		self.error = ''
		self.report = ''
		self.start_at = 0
		self.load()
	
	def dump(self):
		print( 'reconstruct_info' )
		print( '\t'+'cache:', self.cache )
		print( '\t'+'hd:', self.hd )
		print( '\t'+'size:', self.size )
		print( '\t'+'grid:', self.grid )
		print( '\t'+'start_at:', self.start_at )
		print( '\t'+'resolutions:', len(self.resolutions) )
		for r in self.resolutions:
			print( '\t\t', r )
		print( '\t'+'tiles:', len(self.tiles) )
		print( 'errors:', self.error[:-1] )
	
	def open_image(self,ipath):
		output =  None
		try:
			output = Image.open(ipath)
		except:
			self.error += 'failed to open tile:' + self.tiles[0]+'\n' 
			output = None
		return output
	
	def get_real_size(self):
		self.size = [0,0]
		for i in range(0,self.grid[0]):
			tid = self.start_at + i
			im = self.open_image(self.tiles[tid])
			if im == None:
				return False
			self.size[0] += im.size[0]
			im.close()
		for i in range(0,self.grid[1]):
			tid = self.start_at + i*self.grid[0]
			im = self.open_image(self.tiles[tid])
			if im == None:
				return False
			self.size[1] += im.size[1]
			im.close()
		return True
	
	def next_resolution(self):
		
		prev_size = [self.size[0],self.size[1]]
		ratio = self.size[0]/self.size[1]
		self.size = [self.size[0]*2,self.size[1]*2]
		self.grid = [math.ceil((self.size[0]+1)/TILE),math.ceil((self.size[1]+1)/TILE)]
		
		# is it realistic?
		available_tiles = len(self.tiles) - self.start_at
		grid_tiles = self.grid[0] * self.grid[1]
		if available_tiles < grid_tiles:
			# try several solutions:
			tmp0 = (self.grid[0]-1) * self.grid[1]
			tmp1 = self.grid[0] * (self.grid[1]-1)
			tmp2 = (self.grid[0]-1) * (self.grid[1]-1)
			tmp3 = (self.grid[0]-2) * (self.grid[1]-1)
			tmp4 = (self.grid[0]-1) * (self.grid[1]-2)
			tmp5 = (self.grid[0]-2) * (self.grid[1]-2)
			if tmp0 == available_tiles:
				self.grid[0] -= 1
			elif tmp1 == available_tiles:
				self.grid[1] -= 1
			elif tmp2 == available_tiles:
				self.grid[0] -= 1
				self.grid[1] -= 1
			elif tmp3 == available_tiles:
				self.grid[0] -= 2
				self.grid[1] -= 1
			elif tmp4 == available_tiles:
				self.grid[0] -= 1
				self.grid[1] -= 2
			elif tmp5 == available_tiles:
				self.grid[0] -= 2
				self.grid[1] -= 2
			else:
				self.error += 'not enough tiles to reconstruct '+str(self.grid[0])+'x'+str(self.grid[1])+'\n'
				return False
		
		# checking real size
		if not self.get_real_size():
			return False
		
		# checking new ratio:
		nratio = self.size[0]/self.size[1]
		# if new ration does not match previous, let's try some strategies:
		if abs(ratio-nratio) > 0.01:
			diff = [ self.size[0]-(prev_size[0]*2), self.size[1]-(prev_size[1]*2) ]
			re_compute = True
			if diff[0] < -4:
				self.grid[0] += 1
			elif diff[0] > 4:
				self.grid[0] -= 1
			elif diff[1] < -4:
				self.grid[1] += 1
			elif diff[1] > 4:
				self.grid[1] -= 1
			else:
				# it's an acceptable error margin
				re_compute = False
			if re_compute:
				if not self.get_real_size():
					return False
				nratio = self.size[0]/self.size[1]
				if abs(ratio-nratio) > 0.01:
					self.error += 'failed retrieve resolution '+str(self.grid[0])+'x'+str(self.grid[1])+', diff: '+str(diff[0])+'x'+str(diff[1])+', ratio: '+str(ratio)+' vs '+str(nratio)+'\n'
					return False
		
		self.resolutions.append( [ self.start_at, self.size, self.grid ] )
		self.start_at += self.grid[0] * self.grid[1]
		return self.start_at < len(self.tiles)
	
	def load(self):
		
		# collect tiles
		fs = os.listdir(self.cache)
		fs.sort()
		awaited_id = 0
		
		for f in fs:
			if f[0] == '.' or f[-4:] != '.jpg':
				continue
			# getting the last 5 characters of the file
			fi = f[-9:-4]
			awaited_id += 1
			if int(fi) != awaited_id:
				self.error += 'failed to find tile:' + fi+'\n'
				self.tiles = []
				break
			fp = os.path.join(self.cache,f)
			self.tiles.append( fp )
		
		if len(self.tiles) == 0:
			return
		
		# extracting first image
		self.tiles.sort()
		im = self.open_image(self.tiles[0])
		if im == None:
			self.error += 'failed to open tile:' + self.tiles[0]+'\n'
			return
		self.size = im.size
		self.resolutions.append( [ self.start_at, self.size, self.grid ] )
		im.close()
		
		if len(self.tiles) == 1:
			return
		
		self.start_at = 1
		succes = self.next_resolution()
		while succes:
			succes = self.next_resolution()
		
		if self.start_at < len(self.tiles):
			self.error += 'leftover tiles '+str(len(self.tiles)-self.start_at)+'\n'
	
	def generate(self, rid):
		
		res = self.resolutions[rid]
		self.start_at = res[0]
		self.size = res[1]
		self.grid = res[2]
		
		ow = str(self.size[0])
		while len(ow) < 5:
			ow = '0'+ow
		oh = str(self.size[1])
		while len(oh) < 5:
			oh = '0'+oh
		oname = self.unique+'_'+ow+'x'+oh+TARGET_EXT
		opath = os.path.join(self.hd, oname)
		
		if not os.path.exists(opath):
			
			print('generation of '+oname)
			
			try:
				output = Image.new( mode="RGB", size=self.size )
			except:
				self.report += 'failed to create output size:'+str(self.size[0])+'x'+str(self.size[1])+', grid:'+str(self.grid[0])+'x'+str(self.grid[1])+'\n'
				return
			
			for y in range(0,self.grid[1]):
				for x in range(0,self.grid[0]):
					im = self.open_image( self.tiles[self.start_at] )
					if im == None:
						self.report += 'failed to open tile:' + self.tiles[self.start_at]+'\n'
						return
					self.start_at += 1
					imc = im.copy()
					output.paste(imc, (x*TILE,y*TILE) )
			
			try:
				output.save( opath )
				output.close()
			except:
				self.report += 'failed to save output:'+opath+'\n'
				return
			
			self.report += oname+' generated\n'
			self.report += '\t'+'resolution: '+ str(self.size[0])+' x '+str(self.size[1])+'\n'
			self.report += '\t'+'grid: '+ str(self.grid[0])+' x '+str(self.grid[1])+'\n'
		
		else:
			self.start_at += (self.grid[0]*self.grid[1])
			self.report += oname+' exists\n'
			self.report += '\t'+'resolution: '+ str(self.size[0])+' x '+str(self.size[1])+'\n'
			self.report += '\t'+'grid: '+ str(self.grid[0])+' x '+str(self.grid[1])+'\n'
	
	def process(self):
		
		if not os.path.exists(self.hd):
			os.makedirs(self.hd)
		
		ep = os.path.join(self.hd, 'error')
		if len(self.error) > 0:
			with open(ep,'w') as f:
				f.write(self.error)
				f.close()
			return
		elif os.path.exists(ep):
			os.remove(ep)
		
		if len(self.resolutions) == 0:
			return
		
		self.report = ''
		
		# compute resolutions
		for i in range(0,len(self.resolutions)):
			self.generate(i)
		
		self.report += 'total tiles used: '+str(self.start_at)+'/'+str(len(self.tiles))+'\n'
		rp = os.path.join(self.hd, 'report')
		with open(rp,'w') as f:
			f.write(self.report)
			f.close()

if not os.path.exists(TARGET):
	os.makedirs(TARGET)

dir_list = os.listdir(SOURCE)
dir_list.sort()
blueprints = []
recon_report = ''

# searching done folders in cache
recon_report += '# LOADING\n'
for d in dir_list:
	if len(FILTER) > 0 and d.find(FILTER) == -1:
		continue
	dp = os.path.join(SOURCE, d )
	if not os.path.isdir(dp):
		continue
	done_path = os.path.join(dp, 'done')
	if os.path.exists(done_path):
		ri = reconstruct_info(d)
		if len(ri.error) > 0:
			recon_report += '## '+dp+'\n'
			recon_report += ri.error
		else:
			blueprints.append(ri)

recon_report += '# PROCESSING\n'
for bp in blueprints:
	bp.process()
	if len(bp.error) > 0:
		recon_report += '## '+dp+'\n'
		recon_report += ri.error

rp = os.path.join(TARGET, 'report' )
with open(rp,w) as f:
	f.write(recon_report)

# HTML CATALOG

THUMBNAIL = 1

html = ''
html += '<html xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>'
html += '<style type="text/css">'
html += 'body { font: 14px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif; background: #eee; }'
html += 'h1 { margin: 0; padding: 0; }'
html += '.shortcuts, .shortcuts li { list-style: none; margin: 0; padding: 0; }'
html += '.shortcuts { margin-bottom: 10px; }'
html += '.shortcuts li { margin: 2px; display: inline-block; }'
html += '.shortcuts a {text-decoration:none;display:block;background:#f00;color:#fff;font-weight:bold;padding:3px 5px;}'
html += '.shortcuts a img {position:fixed;display:none;left:50%;margin: 25px 0 0 0;float: left;right: 50%;border: 5px solid #f00;}'
html += '.shortcuts a:visited {background:#a00;}'
html += '.shortcuts a:hover {background:#fff;color:#f00;}'
html += '.shortcuts a:hover img {display:block;}'
html += 'table { border: 1px solid #000; width: 95vw; background:#fff; border-collapse:collapse; }'
html += 'tr {padding:0;margin:0;}'
html += 'th {text-align:left; background:#000;color:#fff;}'
html += 'th h3 {margin: 0; padding:5px;}'
html += 'td {vertical-align:top;padding:10px;}'
html += 'td.thumb {text-align:center;vertical-align:middle;background:#999; }'
html += 'th h3 a {text-decoration:none;color:#fff;}'
html += 'pre {background:#ddd;padding:5px;font_size:11px;border:1px solid #000;}'
html += '</style>'
html += '</head><body>'
html += '<table><tr>'
html += '<td><img width="64px" src="assets/search_arch_be.icon.png"/></td>'
html += '<td width="100%">'
html += '<h1>search.arch.be catalog</h1>'
html += 'source: <a href="https://gitlab.com/frankiezafe/search.arch.be">https://gitlab.com/frankiezafe/search.arch.be</a>'
html += '</td>'
html += '</tr></table>'

dir_list = os.listdir(TARGET)
dir_list.sort()
for d in dir_list:
	html += '<a name="'+d+'"></a><h2>reference: '+d+'</h2>'
	shortcuts = ''
	shortcuts += '<ul class="shortcuts"><li>shortcuts:</li>'
	subhtml = ''
	subhtml += '<table>'
	dp = os.path.join( TARGET, d )
	bp_list = os.listdir(dp)
	bp_list.sort()
	for bp in bp_list:
		if not os.path.isdir(os.path.join( TARGET, d, bp )):
			continue
		file_list = os.listdir(os.path.join( TARGET, d, bp ))
		subl = []
		for f in file_list:
			if f[-4:] == '.jpg':
				subl.append( os.path.join( TARGET, d, bp, f ) )
		if len(subl) > 0:
			subl.sort()
			bp_txt = bp
			while bp_txt[0] == '0':
				bp_txt = bp_txt[1:]
			shortcuts += '<li><a href="#'+d+'_'+bp+'">'+bp_txt+'<img src="'+subl[0]+'"/></a></li>'
			subhtml += '<tr><th colspan="3"><a name="'+d+'_'+bp+'"></a><h3><a href="#'+d+'_'+bp+'">'+bp+'</a></h3></th></tr>'
			subhtml += '<tr>'
			subhtml += '<td class="thumb">'
			subhtml += '<a href="'+subl[len(subl)-1]+'">'
			subhtml += '<img src="'+subl[THUMBNAIL]+'"></a></td>'
			subhtml += '<td width="100%">'
			subhtml += 'resolutions:'
			subhtml += '<ul>'
			for i in range(0,len(subl)):
				parts = subl[i].split('_')
				subhtml += '<li><a href="'+subl[i]+'">' + parts[len(parts)-1] + '</a></li>'
			subhtml += '</ul>'
			subhtml += '<a href="'+os.path.join(SOURCE, d+'_'+bp)+'">cache</a><br/>'
			subhtml += '<a href="#'+d+'">back to '+d+'</a>'
			subhtml += '</td>'
			subhtml += '<td>report:'
			rp = os.path.join( TARGET, d, bp, 'report' )
			if os.path.exists(rp):
				f = open(rp, 'r')
				subhtml += '<pre>'+f.read()+'</pre>'
			subhtml += '</td>'
			subhtml += '</tr>'
	shortcuts += '</ul>'
	subhtml += '</table>'
	html += shortcuts
	html += subhtml
	

html += '</body></html>'
with open( 'catalog.html', 'w' ) as f:
	f.write(html)

'''
for bp in blueprints:
	bp_path = os.path.join(SOURCE, bp)
	parts = bp.split('_')
	tgt_path = os.path.join(TARGET, parts[0]+'_'+parts[1]+'_'+parts[2], parts[3])
	if not os.path.exists(tgt_path):
		os.makedirs(tgt_path)
	# getting thumbnail
	thmb = os.path.join(bp_path, '00001.jpg')
	ISIZE = [0,0]
	COLUMNS = 0
	if os.path.exists(thmb):
		im = Image.open(thmb)
		imc = im.copy()
		im.close()
		ISIZE = imc.size
		ow = str(ISIZE[0])
		while len(ow) < 5:
			ow = '0'+ow
		oh = str(ISIZE[1])
		while len(oh) < 5:
			oh = '0'+oh
		imc.save( os.path.join(tgt_path, bp+'_'+ow+'x'+oh+'.jpg') )
		imc.close()
		COLUMNS = 2
	
	reconstruct_report = ''
	START_AT = 0
	
	# searching for a manual configuration
	conf = os.path.join(bp_path, 'reconstruct')
	if os.path.exists(conf):
		START_AT = 2
		c = open(conf, 'r')
		confs = c.read().split('\n')
		for t in confs:
			
			if len(t) == 0:
				continue
			grid_s = t.split('x')
			if len(grid_s) != 2:
				continue
			
			COLUMNS = int(grid_s[0])
			ROWS = int(grid_s[1])
			
			OUPUT_WIDTH = 0
			OUPUT_HEIGHT = 0
			keep_on = True
			
			for i in range(START_AT, START_AT+COLUMNS):
				n = str(i)
				while len(n) < 5:
					n = '0'+n
				ipath = os.path.join(bp_path, n+'.jpg')
				if not os.path.exists(ipath):
					reconstruct_report += 'failed to open: ' + ipath + '\n'
					keep_on = False
					break
				try:
					im = Image.open(ipath)
					OUPUT_WIDTH += im.size[0]
					im.close()
				except:
					reconstruct_report += 'failed to open: ' + ipath + '\n'
					keep_on = False
					break
			
			if keep_on == True:
				for i in range(0,ROWS):
					n = str(START_AT+i*COLUMNS)
					while len(n) < 5:
						n = '0'+n
					ipath = os.path.join(bp_path, n+'.jpg')
					if not os.path.exists(ipath):
						reconstruct_report += 'failed to open: ' + ipath + '\n'
						keep_on = False
						break
					try:
						im = Image.open(ipath)
						OUPUT_HEIGHT += im.size[1]
						im.close()
					except:
						reconstruct_report += 'failed to open: ' + ipath + '\n'
						keep_on = False
						break
			
			if keep_on == True:
				ow = str(OUPUT_WIDTH)
				while len(ow) < 5:
					ow = '0'+ow
				oh = str(OUPUT_HEIGHT)
				while len(oh) < 5:
					oh = '0'+oh
				opath = os.path.join(tgt_path, bp+'_'+ow+'x'+oh+'.jpg')
				if not os.path.exists(opath):
					error = ''
					output = Image.new( mode="RGB", size=(OUPUT_WIDTH,OUPUT_HEIGHT) )
					yoffset = 0
					index = 0
					for r in range(0,ROWS):
						xoffset = 0
						isize = [0,0]
						for c in range(0,COLUMNS):
							n = str(START_AT+index)
							while len(n) < 5:
								n = '0'+n
							ipath = os.path.join(bp_path, n+'.jpg')
							if not os.path.exists(ipath):
								reconstruct_report += 'failed to open: ' + ipath + '\n'
								error = ipath + ' does not exists'
								keep_on = False
								break
							im = None
							try:
								im = Image.open(ipath)
							except:
								reconstruct_report += 'failed to open: ' + ipath + '\n'
								error = ipath + ' failed to open'
								keep_on = False
								break
							isize = im.size
							imc = im.copy()
							output.paste(imc, (xoffset,yoffset) )
							xoffset += isize[0]
							index += 1
						yoffset += isize[1]
					
					info = ''
					info += 'width:'+str(OUPUT_WIDTH)+'\n'
					info += 'height:'+str(OUPUT_HEIGHT)+'\n'
					info += 'start:'+str(START_AT)+'\n'
					info += 'end:'+str(START_AT+(COLUMNS*ROWS)-1)+'\n'
					info += 'columns:'+str(COLUMNS)+'\n'
					info += 'rows:'+str(ROWS)+'\n'
					
					if keep_on == False:
						epath = opath+'.error.txt'
						with open(epath,'w') as f:
							f.write(error+'\n')
							f.write(info)
						opath += '.error.jpg'
					else:
						epath = opath+'.info.txt'
						with open(epath,'w') as f:
							f.write(info)
					output.save( opath )
					output.close()
					print( opath, 'generated' )
			
			reconstruct_report += str(COLUMNS)+'x'+str(ROWS)+'\n'
			ISIZE = [OUPUT_WIDTH,OUPUT_HEIGHT]
			START_AT += COLUMNS*ROWS
		COLUMNS = 0
	
	# let's use automated process
	
	if COLUMNS != 0:
		
		START_AT = 2
		keep_on = True
		
		while( keep_on ):
			
			OUPUT_WIDTH = 0
			OUPUT_HEIGHT = 0
		
			for i in range(START_AT,START_AT+COLUMNS):
				n = str(i)
				while len(n) < 5:
					n = '0'+n
				ipath = os.path.join(bp_path, n+'.jpg')
				if not os.path.exists(ipath):
					reconstruct_report += 'failed to open: ' + ipath + '\n'
					keep_on = False
					break
				try:
					im = Image.open(ipath)
					OUPUT_WIDTH += im.size[0]
					im.close()
				except:
					reconstruct_report += 'failed to open: ' + ipath + '\n'
					keep_on = False
					break
			
			if keep_on == False:
				break
			
			# deducing height
			OUPUT_HEIGHT = int( OUPUT_WIDTH*ISIZE[1]/ISIZE[0] )
			ROWS = math.ceil(OUPUT_HEIGHT/256)
			# is there enough images for this division?
			lasti = START_AT+(COLUMNS*ROWS)-1
			while lasti > START_AT:
				n = str(lasti)
				while len(n) < 5:
					n = '0'+n
				if os.path.exists(os.path.join(bp_path, n+'.jpg')):
					break
				lasti -= 1
			
			if lasti != (START_AT+(COLUMNS*ROWS)-1):
				# not enough tiles to reconstruct the image!
				reconstruct_report += 'not enough images to reconstruct the image: '+str(COLUMNS)+'x'+str(ROWS)+'\n'
				keep_on = False
			
			if keep_on == False:
				break
			
			# recomputing height
			prevh = OUPUT_HEIGHT
			OUPUT_HEIGHT = 0
			for i in range(0,ROWS):
				n = str(START_AT+i*COLUMNS)
				while len(n) < 5:
					n = '0'+n
				ipath = os.path.join(bp_path, n+'.jpg')
				if not os.path.exists(ipath):
					reconstruct_report += 'failed to open: ' + ipath + '\n'
					keep_on = False
					break
				try:
					im = Image.open(ipath)
					OUPUT_HEIGHT += im.size[1]
					im.close()
				except:
					reconstruct_report += 'failed to open: ' + ipath + '\n'
					keep_on = False
					break
			
			if keep_on == False:
				break
			
			if abs(OUPUT_HEIGHT - prevh) >= 10:
				# too much columns, let's try with one less
				COLUMNS -= 1
				if COLUMNS <= 0:
					keep_on = False
				continue
			
			if keep_on == False:
				break
			
			ow = str(OUPUT_WIDTH)
			while len(ow) < 5:
				ow = '0'+ow
			oh = str(OUPUT_HEIGHT)
			while len(oh) < 5:
				oh = '0'+oh
			opath = os.path.join(tgt_path, bp+'_'+ow+'x'+oh+'.jpg')
			
			if not os.path.exists(opath):
				
				error = ''
				output = Image.new( mode="RGB", size=(OUPUT_WIDTH,OUPUT_HEIGHT) )
				yoffset = 0
				index = 0
				for r in range(0,ROWS):
					xoffset = 0
					isize = [0,0]
					for c in range(0,COLUMNS):
						n = str(START_AT+index)
						while len(n) < 5:
							n = '0'+n
						ipath = os.path.join(bp_path, n+'.jpg')
						if not os.path.exists(ipath):
							reconstruct_report += 'failed to open: ' + ipath + '\n'
							error = ipath + ' does not exists'
							keep_on = False
							break
						im = None
						try:
							im = Image.open(ipath)
						except:
							reconstruct_report += 'failed to open: ' + ipath + '\n'
							error = ipath + ' failed to open'
							keep_on = False
							break
						isize = im.size
						imc = im.copy()
						output.paste(imc, (xoffset,yoffset) )
						xoffset += isize[0]
						index += 1
					yoffset += isize[1]
				
				info = ''
				info += 'width:'+str(OUPUT_WIDTH)+'\n'
				info += 'height:'+str(OUPUT_HEIGHT)+'\n'
				info += 'start:'+str(START_AT)+'\n'
				info += 'end:'+str(START_AT+(COLUMNS*ROWS)-1)+'\n'
				info += 'columns:'+str(COLUMNS)+'\n'
				info += 'rows:'+str(ROWS)+'\n'
				
				if keep_on == False:
					epath = opath+'.error.txt'
					with open(epath,'w') as f:
						f.write(error+'\n')
						f.write(info)
					opath += '.error.jpg'
				else:
					epath = opath+'.info.txt'
					with open(epath,'w') as f:
						f.write(info)
				output.save( opath )
				output.close()
				print( opath, 'generated' )
			
			reconstruct_report += str(COLUMNS)+'x'+str(ROWS)+'\n'
			ISIZE = [OUPUT_WIDTH,OUPUT_HEIGHT]
			START_AT += COLUMNS*ROWS
			COLUMNS *= 2
	
	jpegs = 0
	fs = os.listdir(bp_path)
	for f in fs:
		if f[-4:] == '.jpg' and f[0] != '.':
			jpegs += 1
	reconstruct_report += 'image used: '+str(START_AT-1)+'/'+str(jpegs)+'\n'
	
	with open( os.path.join(tgt_path,'report'), 'w' ) as f:
		f.write(reconstruct_report)
'''
